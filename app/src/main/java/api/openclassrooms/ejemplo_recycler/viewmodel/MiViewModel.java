package api.openclassrooms.ejemplo_recycler.viewmodel;

import androidx.lifecycle.ViewModel;

import api.openclassrooms.ejemplo_recycler.data.Repository;

public class MiViewModel extends ViewModel {

    private Repository repository;

    public MiViewModel() {

        repository = Repository.getRepository();

    }

    //  Llega el departamento, que es la selección en nuestro Spinner de MainActivity
    public void getContactosDepartamento(String seleccion) {

            repository.getContactosDepartamento(seleccion);

    }
}
