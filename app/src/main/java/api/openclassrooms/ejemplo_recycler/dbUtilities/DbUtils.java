package api.openclassrooms.ejemplo_recycler.dbUtilities;

// Constantes en lo referente a nuestra tabla Usuario
public class DbUtils {

    public static final String IDUSUARIO = "idUsuario";
    public static final String NOMBRE = "nombre";
    public static final String TELEFONO = "telefono";

    public static final String NOMBRE_TABLA = "usuarios";

    public static final String CREAR_TABLA_USUARIO =
            "CREATE TABLE " + NOMBRE_TABLA + " (" + IDUSUARIO + " INTEGER, " + NOMBRE + " VARCHAR(40), " + TELEFONO + " VARCHAR(15))";
}
