package api.openclassrooms.ejemplo_recycler.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import api.openclassrooms.ejemplo_recycler.MainActivity;
import api.openclassrooms.ejemplo_recycler.R;

public class IntroAustria extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_austria);
    }

    public void arrancar(View view) {

        Intent intent = new Intent(this.getApplicationContext(), MainActivity.class);
        startActivity(intent);

        Toast toast = Toast.makeText(getApplicationContext(), "Bienvenid@", Toast.LENGTH_SHORT);
        toast.show();
    }
}
