package api.openclassrooms.ejemplo_recycler;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class PesoEditar extends AppCompatActivity {

    TextView tvPeso, tvFecha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peso_editar);

//      Capturamos la información que nos viene del métod OnClickListener definido en la clase PesoAdaptador
        String peso = getIntent().getStringExtra("PESO");
        String fecha = getIntent().getStringExtra("FECHA");

        tvFecha = findViewById(R.id.fecha);
        tvFecha.setText(fecha);

        tvPeso = findViewById(R.id.peso);
        tvPeso.setText(peso);
    }

    // Método del botón VOLVER que nos retorna al Recycler al haber hecho click en el detalle list_item
    public void volverAlMain(View view) {

        Intent intent = new Intent(this.getApplicationContext(), MainActivity.class);
        startActivity(intent);

    }
}
