package api.openclassrooms.ejemplo_recycler;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import api.openclassrooms.ejemplo_recycler.data.Repository;
import api.openclassrooms.ejemplo_recycler.model.Peso;
import api.openclassrooms.ejemplo_recycler.viewmodel.MiViewModel;

public class MainActivity extends AppCompatActivity {

//  Instancia de nuestra BBDD
    ConexionSQLiteHelper conn = new ConexionSQLiteHelper(this, "bd_usuarios", null, 1);

    private List<Peso> pesos;
    private RecyclerView listaPesos;

    Spinner spinner;
    private MiViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//      Obtenemos la instancia del Repositorio
        Repository.get(getApplicationContext());

        listaPesos = findViewById(R.id.recycler_ejemplo);
        spinner = findViewById(R.id.depart_spinner);

        viewModel = ViewModelProviders.of(this).get(MiViewModel.class);

        cargaSpinner();
        cargaDatos();
        iniciaAdaptador();

        //      Queremos que el Fragment avise al ViewModel de que elemento ha sido seleccionado
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                viewModel.getContactosDepartamento(adapterView.getItemAtPosition(i).toString());

                Toast toast = Toast.makeText(getApplicationContext(),
                        "Se ha pulsado el elemento " + adapterView.getItemAtPosition(i), Toast.LENGTH_LONG);
                toast.show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void cargaSpinner() {

        spinner.setPrompt("Elige un departamento");
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.spinner_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

    private void iniciaAdaptador() {

        PesoAdaptador adaptador = new PesoAdaptador(pesos);

        LinearLayoutManager lin = new LinearLayoutManager(this);
        lin.setOrientation(LinearLayoutManager.VERTICAL);

        listaPesos.setAdapter(adaptador);
        listaPesos.setLayoutManager(lin);
    }

    // Hardcodeamos los datos a mano, ya que no los estamos obteniendo de una API o BBDD
    public void cargaDatos() {
        pesos = new ArrayList<>();
        pesos.add(new Peso("01/01/2020", "90"));
        pesos.add(new Peso("04/01/2020", "82"));
        pesos.add(new Peso("12/01/2020", "85"));
        pesos.add(new Peso("15/01/2020", "91"));
        pesos.add(new Peso("23/01/2020", "89"));
        pesos.add(new Peso("28/01/2020", "80"));
        pesos.add(new Peso("30/01/2020", "94"));
    }

    // Método del botón VOLVER que nos retorna al Recycler al haber hecho click en el detalle list_item
    public void vesAlFormSQL(View view) {

        Intent intent = new Intent(this.getApplicationContext(), FormularioSqlite.class);
        startActivity(intent);

    }

}
