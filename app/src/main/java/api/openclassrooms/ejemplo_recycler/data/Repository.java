package api.openclassrooms.ejemplo_recycler.data;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

// Patrón de diseño: SINGLETON, el objetivo es asegurar que sólo instanciaremos un objeto de la clase
public class Repository {

    @SuppressLint("StaticFieldLeak")
    private static Repository repository;
    private Context context;

    private Repository(Context context) {
        this.context = context;
    }

    public static Repository get(Context context) {

        if (repository == null) {
            repository = new Repository(context);
        }

        return repository;
    }

    public static Repository getRepository() {
        return repository;
    }

//  Se ejecuta al llamar el método de mismo nombre en nuestro viewModel 'MiViewModel'
    public void getContactosDepartamento(String seleccion) {

        System.out.println("Estamos en el Repositorio, buscamos: " + seleccion);
        MiHilo thread = new MiHilo();
        thread.execute("https://crud-525a3.firebaseio.com/Persona.json");
    }

    public class MiHilo extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... strings) {

            HttpURLConnection connection;
            URL url;
            connection = null;
            String result;
            result ="";

            try{

                url = new URL(strings[0]);
                connection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = connection.getInputStream();

                int data = inputStream.read();

                while(data != -1){
                    result += (char) data;
                    data = inputStream.read();
                }

            }catch (Exception e){

                e.printStackTrace();

            }

            Log.i("RESULT", result);

            return result;
        }

//      El parámetro data es el return que nos llega del método doInBackGround
        @Override
        protected void onPostExecute(String data) {
            super.onPostExecute(data);

//            TODO: Formatear los datos que nos llegan en JSON

        }
    }
}
