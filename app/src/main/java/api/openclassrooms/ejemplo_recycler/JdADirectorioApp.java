package api.openclassrooms.ejemplo_recycler;

import android.app.Application;

import api.openclassrooms.ejemplo_recycler.data.Repository;

public class JdADirectorioApp extends Application {

    public void onCreate() {

        super.onCreate();
        Repository.get(getApplicationContext());
    }

    public void onTerminate() {

        super.onTerminate();
    }
}
