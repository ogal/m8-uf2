package api.openclassrooms.ejemplo_recycler;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import api.openclassrooms.ejemplo_recycler.dbUtilities.DbUtils;

public class ConsultarUsuario extends AppCompatActivity {

    EditText campoId, campoNombre, campoTelefono;

    // Esta clase se encarga de la instancia a nuestra BD
    ConexionSQLiteHelper conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_usuario);

        // Le pasamos los parametros de nuestra BD
        conn = new ConexionSQLiteHelper(getApplicationContext(), "bd_usuarios", null, 1);

        campoId = (EditText) findViewById(R.id.documentoId);
        campoNombre = (EditText) findViewById(R.id.campoNombreConsulta);
        campoTelefono = (EditText) findViewById(R.id.campoTelefonoConsulta);

    }

    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnConsultar:
//                consultar();
                consultarSql();
                break;
            case R.id.btnActualizar:
                actualizarUsuario();
                break;
            case R.id.btnEliminar:
                eliminarUsuario();
                break;
        }

    }

    private void eliminarUsuario() {

        SQLiteDatabase db = conn.getWritableDatabase();
        String[] parametros = {campoId.getText().toString()};


        db.delete(DbUtils.NOMBRE_TABLA, DbUtils.IDUSUARIO + "=?", parametros);

        campoId.setText(""); // Nos limpia el id del Formulario
        limpiar(); // Nos limpia los campos Nombre y Tel. del formulario

        db.close();

        Toast.makeText(getApplicationContext(), "Ya se Eliminó el usuario", Toast.LENGTH_LONG).show();
    }

    private void actualizarUsuario() {

        SQLiteDatabase db = conn.getWritableDatabase();
        String[] parametros = {campoId.getText().toString()};

        // Funciona como un Hash Map, se añaden los campos con el estilo clave-valor
        ContentValues values = new ContentValues();
        values.put(DbUtils.NOMBRE, campoNombre.getText().toString());
        values.put(DbUtils.TELEFONO, campoTelefono.getText().toString());

        // UPDATE Usuarios SET <values de la tabla> WHERE idUsuario = <parametros>
        db.update(DbUtils.NOMBRE_TABLA, values, DbUtils.IDUSUARIO + "=?", parametros);

        Toast.makeText(getApplicationContext(), "Ya se actualizó el usuario", Toast.LENGTH_LONG).show();
        db.close();

    }

    private void consultarSql() {

        // Instancia de lectura de nuestra BD
        SQLiteDatabase db = conn.getReadableDatabase();

        // El campo a través del cual voy a hacer la consulta
        String[] parametros = {campoId.getText().toString()};

        try {
            //select nombre,telefono from usuario where codigo=?
            Cursor cursor = db.rawQuery("SELECT " + DbUtils.NOMBRE + "," + DbUtils.TELEFONO +
                    " FROM " + DbUtils.NOMBRE_TABLA + " WHERE " + DbUtils.IDUSUARIO + " =? ", parametros);

            cursor.moveToFirst();
            campoNombre.setText(cursor.getString(0));
            campoTelefono.setText(cursor.getString(1));

            cursor.close();

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "El documento no existe", Toast.LENGTH_LONG).show();
            limpiar();
        }
    }

    private void consultar() {

        SQLiteDatabase db = conn.getReadableDatabase();
        String[] parametros = {campoId.getText().toString()};
        String[] campos = {DbUtils.NOMBRE, DbUtils.TELEFONO};

        try {
            Cursor cursor = db.query(DbUtils.NOMBRE_TABLA, campos, DbUtils.IDUSUARIO + "=?", parametros, null, null, null);
            cursor.moveToFirst();
            campoNombre.setText(cursor.getString(0));
            campoTelefono.setText(cursor.getString(1));
            cursor.close();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "El documento no existe", Toast.LENGTH_LONG).show();
            limpiar();
        }
    }

    private void limpiar() {
        campoNombre.setText("");
        campoTelefono.setText("");
    }
}