package api.openclassrooms.ejemplo_recycler;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import api.openclassrooms.ejemplo_recycler.model.Peso;

// Clase para conectar el componente "list_item" dentro del RecyclerView
public class PesoAdaptador extends RecyclerView.Adapter<PesoAdaptador.PesoViewHolder> {

    private List<Peso> pesos;

    PesoAdaptador(List<Peso> pesos) {
        this.pesos = pesos;
    }

    @NonNull
    @Override
    public PesoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Vemos como es aquí donde estamos pasando la plantilla XML que se duplicará en el Recycler *******
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new PesoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PesoViewHolder pesoViewHolder, int position) {

        Peso peso = pesos.get(position);
        pesoViewHolder.tvFecha.setText(peso.getFecha());
        pesoViewHolder.tvPeso.setText(peso.getPeso());

        double diferencia;
        String diferenciaString;

        if (position != 0) {
            Peso pesoMenosUno = pesos.get(position - 1);
            diferencia = Double.parseDouble(peso.getPeso()) - Double.parseDouble(pesoMenosUno.getPeso());
            diferenciaString = String.valueOf(diferencia);
        } else {
            diferencia = 0;
            diferenciaString = "0";
        }
        pesoViewHolder.tvDiferencia.setText(diferenciaString);
        if (diferencia > 0) {
            pesoViewHolder.tvDiferencia.setTextColor(Color.parseColor("#64DD17"));
        } else if (diferencia == 0) {
            pesoViewHolder.tvDiferencia.setTextColor(Color.parseColor("#000000"));
        } else {
            pesoViewHolder.tvDiferencia.setTextColor(Color.parseColor("#D50000"));
        }
    }

    @Override
    public int getItemCount() {
        return pesos.size();
    }

    //  Definimos nuestra clase interna de tipo ViewHolder y hacemos los elementos del Recycler pulsables
    class PesoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvFecha, tvPeso, tvDiferencia;

        PesoViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvFecha = itemView.findViewById(R.id.tvFecha);
            tvPeso = itemView.findViewById(R.id.tvPeso);
            tvDiferencia = itemView.findViewById(R.id.tvDiferencia);
        }

        @Override
        public void onClick(View view) {

            Context context = view.getContext();
            int posicion = getAdapterPosition();
            Peso peso = pesos.get(posicion);

            Intent intent = new Intent(view.getContext(), PesoEditar.class);
            intent.putExtra("PESO", peso.getPeso());
            intent.putExtra("FECHA", peso.getFecha());
            context.startActivity(intent);
        }
    }

}
