package api.openclassrooms.ejemplo_recycler;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import api.openclassrooms.ejemplo_recycler.dbUtilities.DbUtils;

public class FormularioSqlite extends AppCompatActivity {

    EditText campoId, campoNombre, campoTelefono;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_sqlite);

        campoId = (EditText) findViewById(R.id.campoId);
        campoNombre = (EditText) findViewById(R.id.campoNombre);
        campoTelefono = (EditText) findViewById(R.id.campoTelefono);
    }

    public void onClickRegistroUsuario(View view) {

//      registrarUsuarios();
        registrarUsuarioSql();
    }

    private void registrarUsuarioSql() {

        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(this, "bd_usuarios", null, 1);
        SQLiteDatabase db = conn.getWritableDatabase();

        String insert = "INSERT INTO " + DbUtils.NOMBRE_TABLA +
                "( " + DbUtils.IDUSUARIO + ", " + DbUtils.NOMBRE + ", " + DbUtils.TELEFONO + ")" +
                " VALUES ( " + campoId.getText().toString() + ", '" + campoNombre.getText().toString() + "', '" + campoTelefono.getText().toString() + "')";

        db.execSQL(insert);

        Toast toast = Toast.makeText(getApplicationContext(), "Se ha insertado el usuario " + campoNombre.getText().toString(), Toast.LENGTH_SHORT);
        toast.show();

        db.close();
    }

    //  Ejemplo de inserción con content values
    private void registrarUsuarios() {

        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(this, "bd_usuarios", null, 1);

        // Abrimos la base de datos para poder editarla
        SQLiteDatabase db = conn.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DbUtils.IDUSUARIO, campoId.getText().toString());
        values.put(DbUtils.NOMBRE, campoNombre.getText().toString());
        values.put(DbUtils.TELEFONO, campoTelefono.getText().toString());

        long idResultante = db.insert(DbUtils.NOMBRE_TABLA, DbUtils.IDUSUARIO, values);

        Toast toast = Toast.makeText(getApplicationContext(), "Id de Registro: " + idResultante, Toast.LENGTH_SHORT);
        toast.show();

        db.close();

    }

    public void onClickConsultarUsuario(View view) {

        Intent intent = new Intent(this.getApplicationContext(), ConsultarUsuario.class);
        startActivity(intent);

    }
}